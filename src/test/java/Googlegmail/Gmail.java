package Googlegmail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Gmail {

    public static void main(String[] args) throws InterruptedException {

        WebDriver driver;
        WebElement j1,kl2,f2,g4,f7,p4,f6,c7,m3,po2,lk7;
        System.setProperty("webdriver.chrome.driver","src/test/java/chromedriver_win32/chromedriver.exe");
        ChromeOptions options=new ChromeOptions();
        options.addArguments("--disable-notifications");
        driver=new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(7));
       // WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(7));
        driver.get("https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F%26ogbl%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
        String kp2="//input[contains(@aria-label,'Email')]";
        j1= driver.findElement(By.xpath(kp2));
        j1.click();
        j1.sendKeys("medemomail20@gmail.com");
        String hg34="//span[text()='Next']";
        kl2= driver.findElement(By.xpath(hg34));
        kl2.click();
        String jp2="//input[contains(@aria-label, 'Enter your password')]";
        f2= driver.findElement(By.xpath(jp2));
        f2.click();
        f2.sendKeys("Testingpurpose21");
        String lo5="//span[contains(text(), 'Next')]";
        g4= driver.findElement(By.xpath(lo5));
        g4.click();
        String kn3="//div[contains(text(),'Compose')]";
        f7=driver.findElement(By.xpath(kn3));
        f7.click();
        String jl3="//textarea[contains(@aria-label,'To')]";
        p4=driver.findElement(By.xpath(jl3));
        p4.click();
        p4.sendKeys("arkajit1992@gmail.com");
        String hl37="//div[contains(@aria-label, 'Message Body')]";
        c7= driver.findElement(By.xpath(hl37));
        c7.click();
        String kf34="//input[contains(@placeholder,'Subject')]";
        f6= driver.findElement(By.xpath(kf34));
        f6.click();
        f6.sendKeys("My home practice");
        //String hl37="//div[contains(@aria-label, 'Message Body')]";
        c7= driver.findElement(By.xpath(hl37));
        c7.click();
        c7.sendKeys("Practice at home");
        String gp7="//div[contains(@aria-label, 'Send \u202A(Ctrl-Enter)\u202C')]";
        m3= driver.findElement(By.xpath(gp7));
        m3.click();
        Thread.sleep(4000);
        String kp9="//a[contains(@aria-label,'Google Account: Inceptial Work')]";
        po2= driver.findElement(By.xpath(kp9));
        po2.click();
        String jc7="//a[contains(text(),'Sign out')]";
        lk7= driver.findElement(By.xpath(jc7));
        lk7.click();

    }
}
